NAME=thesis
LATEXMKOPTS=-lualatex #use -pdf for forcing pdflatex
LATEXMK=latexmk $(LATEXMKOPTS)

all:
	$(LATEXMK) $(NAME)

clean:
	$(LATEXMK) -C
