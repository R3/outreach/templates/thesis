
# A nice thesis template

What's included:

- modern packages (biblatex, cleveref, better fonts)
- slightly more useful examples (figures, diagrams, tables, code listings),
  structure hints, some goodies
- Dockerized & CI build options

## CI configuration

The repository contains valid configuration for both *GitLab* CI and the *GitHub* actions.
No matter what GIT hosting you use, you can always download latest version of your thesis right from the artifacts!

## How-to

1. Type `make` and check that everything compiled allright. You should get a `thesis.pdf`.
2. Fill in your details into `metadata.tex`.
3. Look at the example code in chapters 1 to 3 (there are some useful hints), then erase it.
4. Write the thesis.
5. Submit and defend the thesis.

### Installing LaTeX

On most BSD and GNU-style Linux distributions, it should be sufficient to
install some random `texlive-*` packages (and add more if non-standard TeX
functionality is required, or just go for `texlive-full`).

In short, on the most common Linux systems (Ubuntu and Debian derivatives), you
should be able to install full TeXLive distribution by:
```sh
apt-get install texlive-full
```
...and then compile the template by running this command in the template directory:
```sh
make
```

On other operating systems and distributions, installation of LaTeX may be
harder, but should be doable. One of the following methods should work for
virtually any mainstream operating system:

- For a single-user distribution on unix, use the provided [installation script](https://www.tug.org/texlive/quickinstall.html).
- On windows, use [MiKTeX](https://www.tug.org/texlive/windows.html).
- On Mac, use any suitable variant of [MacTeX](https://www.tug.org/mactex/).

### Building in Docker

If you can use Docker on your computer, you can use any of the existing
prebuilt images that contain LaTeX, thus avoiding the (possibly complicated)
LaTeX installation procedure on your system. You can use an image such as:
```sh
docker pull aergus/latex
```

You should then be able to compile the thesis using this command:
```sh
docker run -u $UID -ti --rm -v $PWD:/th -w /th aergus/latex make
```

## Ideas/improvements/more examples?

Pull requests welcome.

## Acknowledgements and licenses

The template is a Uni.Lu-adjusted version of an improved template for MFF CUNI by Mirek Kratochvil (see https://github.com/exaexa/better-mff-thesis). Parts of its code (esp. the frontmatter typesetting) were based on the old MFF CUNI template by Martin Mareš, Arnošt Komárek, and Michal Kulich. Small and useful fixes were coded or pointed out by Vít Kabele, Jan Joneš, Gabriela Suchopárová, Evžen Wybitul, and many others. (Many thanks to everyone involved!)

Any occurring university, department, institute and faculty logos are a property of the respective universities, departments, institutes and faculties.

Everything else in this repository is released into the public domain under the
[CC0 Universal Public Domain Dedication license](https://creativecommons.org/publicdomain/zero/1.0/).
